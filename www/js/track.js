$(document).ready(function(){

    window.PageTracker = (function() {
        var self = {};

        self.keepPage = function(params) {
            $.post('tracker.php', params);
        };

        return self;
    }());

});