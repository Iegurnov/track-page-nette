    var Analitic = (function() {
        var self = {};

        self.load = function(){
            google.charts.load('current', {'packages':['table', 'corechart']});
        };

        self.tableOfPages = function(params, element, par, ele) {
            google.charts.setOnLoadCallback(drawTable);
            function drawTable() {
                var pages = new google.visualization.DataTable();
                pages.addColumn('string', 'Page');
                pages.addColumn('string', 'User');
                pages.addColumn('string', 'Date');
                var rows = [];
                var i;
                for (i in params){
                    var j;
                    var row = [];
                    for (j in params[i]) {
                        row.push(params[i][j]);
                    }
                    rows.push(row);
                }
                pages.addRows(rows);
                var table_pages = new google.visualization.Table(document.getElementById(element));
                table_pages.draw(pages, { showRowNumber: false, width: '100%', height: '100%' });
            }

        };

        self.tableOfUsers = function(params, element){
            google.charts.setOnLoadCallback(drawTable);
            function drawTable() {
                var users = new google.visualization.DataTable();
                users.addColumn('string', 'User');
                var rows=[];
                var i;
                for (i in params){
                    rows.push([params[i]]);
                }
                users.addRows(rows);

                var table_users = new google.visualization.Table(document.getElementById(element));
                table_users.draw(users, { showRowNumber: true, width: '100%', height: '100%' });
            }
        };

        self.chartOfFrequency = function(params, element){
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var data = [];
                data.push(['Page', 'Views per Day']);
                var i;
                for (i in params){
                    var j;
                    var row = [];
                    for (j in params[i]) {
                        row.push(params[i][j]);
                    }
                    data.push(row);
                }
                var perDay = google.visualization.arrayToDataTable(data);

                var optionsPerDay = {
                    pieHole: 0.4,
                    legend: 'yes',
                    chartArea:{ left:0,top:'20%',width:'100%',height:'100%'},
                    title: 'Average number of views per day'
                };

                var chart = new google.visualization.PieChart(document.getElementById(element));
                chart.draw(perDay, optionsPerDay);
            }
        };

        return self;
    }());

