
<?php
session_start();
/** 	Database connection **/
$host= '';
$database='';
$user='';
$pswd='';

$log = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'))->format('Y-m-d H:i:s').": User: ".$_POST['user'].
    "; Page: ".$_POST['page']." #";

$dbhandle = mysql_connect($host, $user, $pswd)
or die("Unable to connect to MySQL");
$log.=" Connected to MySQL ";

$selected = mysql_select_db($database,$dbhandle)
or die("Could not connect to ".$database." database");

/**     If table page_track does not exist, it will be created  **/
if(mysql_num_rows(mysql_query("SHOW TABLES LIKE 'page'"))) {
    $log.="# Table page already exists ";
} else {
    $result = mysql_query("CREATE TABLE page (
      id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      user VARCHAR(255) NOT NULL,
      page VARCHAR(255) NOT NULL,
      date DATE
    ) ");
    $log.="# Table page has been created ";
}

/**     Add page data to page_track table **/
$result = mysql_query("INSERT INTO page (user,page,date)
                        VALUES('{$_POST['user']}','{$_POST['page']}','{$_POST['datetime']}');");
if ($result==TRUE) {
    $log.="# The page ".$_POST['page']." has been tracked successfully ";
} else {
    $log.="# The page ".$_POST['page']." hasn't been tracked to 'page' table ";
}

file_put_contents('log.txt', $log."\n" , FILE_APPEND);

session_register_shutdown();

