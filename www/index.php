<?php

// Uncomment this line if you must temporarily take down your site for maintenance.
// require __DIR__ . '/.maintenance.php';
define('PROJECT_DIR', __DIR__ . '/..');
define('APP_DIR', PROJECT_DIR . '/app');
define('GENERATED_DIR', PROJECT_DIR . '/generated');

$container = require __DIR__ . '/../app/bootstrap.php';

$container->getByType('Nette\Application\Application')->run();
