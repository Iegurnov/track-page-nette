Tracker
================================================

Tracker is a pre-packaged and pre-configured Nette Framework application that you can use as the skeleton for your new
applications.There is an ability to keep track of visited pages and brows some analytics (list of viewed pages, list of
users, chart of visit frequency).

Installing
----------
You can install Tracker with composer:
```
composer create-project iegurnov/tracker -sdev
```
It is necessary to change setting of Database in .neon and www/tracker.php

How to use
----------
To keep track some page you have to add www/js/track.js to template. After that you can write data to database:
```
PageTracker.keepPage(params);
```
where params is { page: 'your_page', datetime:'your_datetime', user:'your_user' }. tracker.php will create 'page' table
in your database if it is not there.

 To browse analytics you have to add www/js/analitic.js to template and
```
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>.
```
Then use:
```
 Analitic.load();
```
 Now you can build a table of visited pages:
```
 Analitic.tableOfPages('params',"element");
```
 'params' = { page: 'your_page', user:'your_user', datetime:'your_datetime', } that you can take from Model by
 getPagesViewed() method of PageFacade.

 A table of users:
```
 Analitic.tableOfUsers('list_of_users',"element");
```
 'list_of_users' - returned by getUsers() method of PageFacade.
```
 Analitic.chartOfFrequency('frequency',"element");
```
 'frequency' - returned by getViewsPerDay() method of PageFacade.
 'element's are ids of correspond divs.



