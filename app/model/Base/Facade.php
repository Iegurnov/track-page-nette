<?php
namespace App\Model\Base;

use Kdyby\Doctrine\EntityManager;

class Facade
{
    /** @var EntityManager */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $entityName
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function findOrCreateEntity($entityName, $id)
    {
        if ($id === NULL) {
            $entity = new $entityName;
            $this->em->persist($entity);
            return $entity;
        }
        $entity = $this->em->getRepository($entityName)->find($id);
        if ($entity === NULL) {
            throw new \Exception('Entity '.$entity.' was not found.');
        }
        return $entity;
    }

}