<?php

namespace App\Model\Page;

use Kdyby;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 **/
class Page extends \Kdyby\Doctrine\Entities\BaseEntity
{
    use Kdyby\Doctrine\Entities\Attributes\Identifier;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $page;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", length=100, nullable=false)
     */
    private $date;

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param string $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

}