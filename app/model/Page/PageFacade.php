<?php

namespace App\Model\Page;

use App\Model\Base\Facade;
use Nette\Utils\DateTime;

class PageFacade extends Facade
{
    /**
     * @param array $data
     * @param int|NULL $id
     * @return Page
     * @throws \Exception
     */
    public function update($data, $id = NULL)
    {
        try {
            $entity = $this->findOrCreateEntity(Page::class, $id);
            $fields = array("page", "date", "user");

            foreach ($fields as $field) {
                if (array_key_exists($field, $data)) {
                    $entity->{$field} = $data[$field];
                }
            }

            $this->em->flush();
            return $entity;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param int|int[] $ids
     * @return bool
     * @throws \Exception
     */
    public function delete($ids)
    {
        if (!is_array($ids)) {
            $ids = array($ids);
        }

        try {
            $this->em->beginTransaction();

            foreach ($ids as $id) {
                $entity = $this->find($id);
                if($entity !== NULL) {
                    $this->em->remove($entity);
                }
            }

            $this->em->flush();
            $this->em->commit();
            return TRUE;

            // referencing to another entity...
        } catch (\Doctrine\DBAL\DBALException $e) {
            throw new \Exception('Entity cannot be deleted. There are some relations.');

        } catch (\Exception $e) {
            $this->em->rollback();
            throw new \Exception('Entity cannot be deleted.');
        }
    }

    /**
     * @param int $id
     * @return Page|NULL
     */
    public function find($id)
    {
        return $this->em->getRepository(Page::class)->find($id);
    }

    /**
     * @return array
     */
    public function getPagesViewed(){
        $list = array();
        $query = $this->em->createQuery('SELECT p FROM App\Model\Page\Page p');
        $pages = $query->getResult();
        if(!empty($pages)) {
            foreach ($pages as $page) {
                array_push($list, array(
                    'page' => $page->getPage(),
                    'user' => $page->getUser(),
                    'date' => $page->getDate()->format('Y-m-d H:i:s')
                ));
            }
        }
        return $list;
    }

    /**
     * @return array
     */
    public function getUsers(){
        $list = array();
        $query = $this->em->createQuery('SELECT DISTINCT p.user FROM App\Model\Page\Page p');
        $users = $query->getResult();
        if(!empty($users)) {
            foreach ($users as $user) {
                array_push($list, $user['user']);
            }
        }
        return $list;
    }

    public function getViewsPerDay()
    {
        $list = array();
        $currentDate = DateTime::createFromFormat('Y-m-d H:i:s', date("Y-m-d H:i:s"));
        $query = $this->em->createQuery("SELECT p.page, MIN(p.date) AS first, COUNT(p) AS number FROM App\Model\Page\Page p GROUP BY p.page");
        $pages = $query->getResult();
        if (!empty($pages)) {
            foreach ($pages as $page) {
                $age = date_diff($currentDate, DateTime::createFromFormat('Y-m-d H:i:s', $page['first']));
                array_push($list, array(
                    'page' => $page['page'],
                    'frequency' => round($page['number']/$age->days, 5)
                ));
            }
        }
        return $list;
    }

}