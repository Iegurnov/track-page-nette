<?php

namespace App\Presenters;

use App\Model\Page\PageFacade;
use Tracy\Debugger;


class TrackPresenter extends BasePresenter
{
    /** @var PageFacade @inject */
    public $pageFacade;

    public function renderTracker()
    {
        $this->template->pages = $this->pageFacade->getPagesViewed();
        $this->template->users = $this->pageFacade->getUsers();
        $this->template->frPerDay = $this->pageFacade->getViewsPerDay();
    }

}